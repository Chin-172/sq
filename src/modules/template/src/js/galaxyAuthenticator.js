// @<COPYRIGHT>@
// ==================================================
// Copyright (c) 2021 Siemens
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@

/**
 * @module js/galaxyAuthenticator
 */

// import from Apollo: the module from Apollo's library
import eventBus from 'js/eventBus';

// import from js: file from Apollo's module
import AwHttpService from 'js/awHttpService';
import AwInjectorService from 'js/awInjectorService';
import AwPromiseService from 'js/awPromiseService';
import appCtxSvc from 'js/appCtxService';
import galaxyFrameUtils from 'js/galaxyFrameUtils';

let $q = AwPromiseService.instance;
let $http = AwHttpService.instance || AwInjectorService.instance.get( '$http' );
const CMM_ORCHESTRATE = '/user';

let exports = {};
let ENV_DATA = null;

/**
 * function to determine if there is already a valid web session or not.
 *
 * @returns {Promise} a promise
 */
export let checkIfSessionAuthenticated = async () => {
    try {
        let getEnvPromise = null;
        if( ENV_DATA === null ) {
            getEnvPromise = galaxyFrameUtils.getEnv();
        } else {
            getEnvPromise = Promise.resolve( ENV_DATA );
        }

        getEnvPromise.then( ( env ) => {
            ENV_DATA = env;

            loginToZeus( ENV_DATA );
        } );
    } catch ( error ) {
        let errorMsg;
        if( isPresent( error.data ) ) {
            errorMsg = error.data;
        } else {
            errorMsg = error;
        }
        throw new Error( errorMsg );
    }
};

/**
 * Login to Zeus
 * @param {Object} envRes - env data
 */
function loginToZeus( envRes ) {
    if( envRes.REQUIRE_LOGIN === 'false' || envRes.REQUIRE_LOGIN === undefined ) {
        let userInfo = {
            name: 'Anonymous',
            email: 'anonymous@unknown.com',
            uid: 'anonymous'
        };
        appCtxSvc.registerCtx( 'userInfo', userInfo );
    } else {
        let query =
            'query {\n                        getLoggedInUserInfo {\n                            user {\n                                uid\n                                email\n                                userName\n                                firstName\n                                lastName\n                                modelType\n                                profileImage\n                            }\n                        }\n                    }';
        performQuery( 'session', query, null ).then( ( queryRes ) => {
            let userInfo = {
                name: queryRes.given_name + ' ' + queryRes.family_name,
                email: queryRes.email,
                uid: queryRes.uid
            };
            appCtxSvc.registerCtx( 'userInfo', userInfo );
        } );
    }
}

/**
 * authenticator specific function to carry out authentication. In the interactive case, we just resolve directly to
 * continue the pipeline.
 *
 * @returns {Object} all export functions
 */
export let getAuthenticator = function() {
    return exports;
};

/**
 * this is called during the authentication process. It gets invoked after the authentication is completed/ready. It
 * is a spot to do any session level initialization.
 *
 * @return {Promise} promise to be resolved after the authenticator does self initialization
 */
export let postAuthInitialization = function() {
    return $q.resolve();
};

/**
 * triggers the authenticator sign out logic. Returns a promise invoked upon completion
 *
 * @return {Promise} promise to be invoked upon completion of the signout tasks
 */
export let signOut = function() {
    return $q.resolve();
};

/**
 * set up the scope values needed by the login view
 */
export let setScope = function() {
    //
};

/**
 * Perform query.
 *
 * @param {string} application the application identifier
 * @param {string} query the query string
 * @param {object} variables the variables to the query
 * @returns {Promise} a promise
 */
const performQuery = async ( application, query ) => {
    eventBus.publish( 'progress.start' );
    let body = {
        application: application,
        query: query
    };
    let config = {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    };

    try {
        let res = await $http.post( CMM_ORCHESTRATE, body, config );
        eventBus.publish( 'progress.end' );
        return res.data;
    } catch ( error ) {
        eventBus.publish( 'progress.end' );
        throw new Error( error );
    }
};

const isPresent = ( obj ) => {
    return undefined !== obj && null !== obj;
};

export default exports = {
    checkIfSessionAuthenticated,
    getAuthenticator,
    postAuthInitialization,
    signOut,
    setScope
};
