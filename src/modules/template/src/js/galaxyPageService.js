// Copyright (c) 2021 Siemens
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@
import eventBus from 'js/eventBus';

// import from js: file from Apollo's module
import appCtxSvc from 'js/appCtxService';
import galaxyFrameUtils from 'js/galaxyFrameUtils';

import { delegator, debugMode, utils, galaxyStringDef, galaxyEventBus } from '@swf3d/utils';

let ENV_DATA = null;
let hasRegisterLogoutEvent = false;
let hasInitLoadOption = false;
const partLoadOption = 'PartiallyLoad';
const componentsToLoad = 'All';
const CTX_TITLE_PART_LOAD_OPTION = 'galaxy.settings.partLoadOption';
const CTX_TITLE_COMPONENTS_TO_LOAD = 'galaxy.settings.componentsToLoad';

let onApiLastTimeout = null;

export const galaxyPageOnInitFunction = async function() {
    try {
        if( ENV_DATA === null ) {
            // eslint-disable-next-line require-atomic-updates
            ENV_DATA = await galaxyFrameUtils.getEnv();
        }
        // Set dev mode to enable debug env.
        debugMode.setDevMode( ENV_DATA.IS_DEV_ENV );
        galaxyFrameUtils.initDebugTools();

        registerUserLogoutEvent();

        // Get atlas init mode.
        document.ATLAS_CFG_FILE = ENV_DATA.ATLAS_CFG_FILE;
        // new: store env in ctx
        // do not need to modify every time
        appCtxSvc.updatePartialCtx( 'galaxy.env', ENV_DATA );

        // Turn on galaxy debug tool if required from enviroment vaialbe.
        const galaxyDebug = ENV_DATA.GALAXY_DEBUG;
        if( utils.isPresent( galaxyDebug ) && true === galaxyDebug ) {
            localStorage.setItem( 'galaxy-debug', 'true' );
            eventBus.publish( 'galaxyFrame.debug.update', { value: true } );
        }

        // If turn on login in env and user didn't login will redirect to landing page.
        if( ENV_DATA.isAuthenticated === 'true' ) {
            if( hasInitLoadOption === false ) {
                hasInitLoadOption = true;
                const atlasConfig = ENV_DATA.ATLAS_CFG_FILE;
                const { isStateless } = atlasConfig;
                if( isStateless ) {
                    let url = `${window.location.origin}/atlasStateless`;

                    let options = {
                        pageSize: atlasConfig.pageSize,
                        url: url,
                        log: {
                            serverURL: ENV_DATA.URL.logServer,
                            logDumpOn: ENV_DATA.LOG_DUMP
                        }
                    };
                    delegator.init( { ...atlasConfig, ...options }, delegator.STATELESS );
                } else {
                    delegator.init( atlasConfig );

                    // Show progress bar on the bottom
                    appCtxSvc.updatePartialCtx( 'galaxy.loading.progress', { title: 'Connecting to websocket', showDots: true, showIndicator: true } );

                    registerOnApiCallback();
                    registerOnConnectErrorCallback();

                    delegator.API.setLoadOptions( partLoadOption, componentsToLoad ).then( () => {
                        appCtxSvc.updatePartialCtx( CTX_TITLE_PART_LOAD_OPTION, partLoadOption );
                        appCtxSvc.updatePartialCtx( CTX_TITLE_COMPONENTS_TO_LOAD, componentsToLoad );
                    } );
                }
            }
        }

        return ENV_DATA;
    } catch ( error ) {
        let errorMsg = utils.isPresent( error.data ) ? error.data : error;
        throw new Error( errorMsg );
    }
};

export const galaxyAuthPageOnInitFunction = function() {
    try {
        if( ENV_DATA.isAuthenticated !== 'true' ) {
            location.href = location.href.replace( '/#', '' );
        }
    } catch ( error ) {
        let errorMsg = utils.isPresent( error.data ) ? error.data : error;
        throw new Error( errorMsg );
    }
};

/**
 * Register the logout event from galaxyFrameUtils, logout when
 *  1. session timeout
 *  2. click logout button in user panel
 */
function registerUserLogoutEvent() {
    if( hasRegisterLogoutEvent === false ) {
        hasRegisterLogoutEvent = true;

        eventBus.subscribe( galaxyStringDef.EVENT_PREFIX_CORE + '.logout', () => {
            sessionStorage.clear();
            window.location.href = window.location.origin.concat( '/logout' );
        } );
    }
}

/**
 * Register onApi callback
 * It is called when the status of websocket changes.
 */
function registerOnApiCallback() {
    galaxyEventBus.subscribe( galaxyStringDef.EVENT_PREFIX_CORE + '.onApi', ( eventData ) => {
        let status = '';

        if( onApiLastTimeout ) {
            clearTimeout( onApiLastTimeout );
        }
        if( eventData === 'CONNECTED' ) {
            status = 'Connect to websocket';
            onApiLastTimeout = setTimeout( () => {
                updateLoadingProgress( 'Connecting to Atlas worker', true, true );
            }, 3000 );
        } else if( eventData === 'connected' ) {
            status = 'Connect to Atlas worker';
            onApiLastTimeout = setTimeout( () => {
                updateLoadingProgress( '', false, false );
            }, 3000 );
        } else if( eventData === 'closed' ) {
            status = 'Websocket closed';
        } else if( eventData === 'error' ) {
            status = 'Websocket connection failed';
        }
        updateLoadingProgress( status, false, false );
    } );
}

/**
 * Register onConnectError callback
 * It is called when atlas init fail.
 */
function registerOnConnectErrorCallback() {
    galaxyEventBus.subscribe( galaxyStringDef.EVENT_PREFIX_CORE + '.onConnectError', ( error ) => {
        updateLoadingProgress( error, false, false );
    } );
}

/**
 * Update props of loading progress
 * 
 * @param {String} title - status of loading progress
 * @param {Boolean} showDots - show dots?
 * @param {Boolean} showIndicator - show indicator?
 */
function updateLoadingProgress( title, showDots, showIndicator ) {
    appCtxSvc.updatePartialCtx( 'galaxy.loading.progress', { title, showDots, showIndicator } );
}

export default {
    galaxyPageOnInitFunction,
    galaxyAuthPageOnInitFunction
};
