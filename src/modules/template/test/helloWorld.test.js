/**
 * Sample Jasmine test
 *
 * @module test/helloWorldTest
 */
import appCtxService from 'js/appCtxService';

//NOTE run tests with `npm run test`

describe( 'Testing HelloWorld', function() {
    beforeEach( function() {
        // initialize logic here to be run before each test
    } );

    // an example of a simple unit test
    it( 'HelloWorld test', function() {
        appCtxService.registerCtx( 'hello', 'world' );
        expect( appCtxService.getCtx( 'hello' ) ).toEqual( 'world' );
    } );
} );
