// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@

/**
 * @module js/helloworldUtils
 */

// import from Apollo: the module from Apollo's library
import appCtxSvc from 'js/appCtxService';

// import from js: file from Apollo's module
import popupSvc from 'js/popupService';
import galaxyWorkspace from 'js/galaxyWorkspace';
import switchViewSvc from 'js/switchViewService';

// import from node_modules
import { delegator } from '@swf3d/utils';

let workspacePopupId = null;

export const openWorkspaceDialog = () => {
    popupSvc.show( {
        declView: 'helloworldWorkspaceDialog',
        locals: {
            caption: 'Workspace',
            anchor: 'closePopupAnchor'
        },
        options: {
            isModal: false,
            clickOutsideToClose: false,
            draggable: true,
            height: '800',
            width: '1200'
        }
    } ).then( ( result ) => {
        workspacePopupId = result.id;
    } );
};

export const closeWorkspaceDialog = () => {
    popupSvc.hide( workspacePopupId );
    workspacePopupId = null;
};

export const init = function() {
    const env = appCtxSvc.getCtx( 'galaxy.env' );
    galaxyWorkspace.init( { URL: env.URL } );
    galaxyWorkspace.collab();
};

export const partSelected = async ( eventData ) => {
    closeWorkspaceDialog();
    let selectedPart = eventData.urn;
    // Show the status bar on the bottom.
    appCtxSvc.updatePartialCtx( 'galaxy.loading.progress', { title: 'Opening part', showDots: true, showIndicator: true } );
    // Send request to Atlas
    delegator.API.openBaseDisplay( selectedPart, false ).then( ( jsonData ) => {
        setTimeout( () => {
            appCtxSvc.updatePartialCtx( 'galaxy.loading.progress', { title: '', showDots: false, showIndicator: false } );
        }, 3000 );
        // Show complete on the status bar
        appCtxSvc.updatePartialCtx( 'galaxy.loading.progress', { title: 'Part opened successfully', showDots: false, showIndicator: false } );
    } ).catch( error => {
        appCtxSvc.updatePartialCtx( 'galaxy.loading.progress', { title: error.message, showDots: false, showIndicator: false } );
    } );
};

/**
 * NSAF-1574. I removed all instances of the closePart UI,
 * but I feel like keeping this closePart function code here is still fine,
 * until the closePart section of the README is updated.
 */
/*
export const closePart = () => {
    // Clear viewer scene
    document.viewerManager.clearScene();

    appCtxSvc.updatePartialCtx( 'loadingProgress.status', { title: 'Closing part', showdots: true, showIndicator: true } );
    eventBus.publish( galaxyStringDef.EVENT_PREFIX_THREEDVIEWER + '.onPartClose' );

    delegator.API.closeAll().then( () => {
        appCtxSvc.updatePartialCtx( 'galaxy.workspace.selectedObject', undefined );
        appCtxSvc.updatePartialCtx( 'galaxy.activeNavigator', undefined ); // This line should be moved to app factory

        appCtxSvc.updatePartialCtx( 'loadingProgress.status', { title: 'Close part success', showdots: false, showIndicator: false } );
        eventBus.publish( 'closeLoadingProgress', 3000 );

        eventBus.publish( galaxyStringDef.EVENT_PREFIX_CORE + '.atlasCloseSuccess' );
        eventBus.publish( galaxyStringDef.EVENT_PREFIX_CORE + '.onClosePartSuccess' );
    } ).catch( ( err ) => {
        console.log( `error: Close all parts; failed : ${err}.` );
    } );
};
*/

export const goToPart = function() {
    let isAuthenticated = appCtxSvc.getCtx( 'galaxy.env.isAuthenticated' );
    if( isAuthenticated === 'true' ) {
        switchViewSvc.switchView( 'part' );
    } else {
        window.location.href = window.location.origin.concat( '/part' );
    }
};

export default {
    openWorkspaceDialog,
    closeWorkspaceDialog,
    goToPart,
    init,
    partSelected
};
