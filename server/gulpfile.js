// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@

const del = require('del');
const gulp = require('gulp');
const typescript = require('gulp-typescript');
const gulpDebug = require('gulp-debug');
const sourcemaps = require('gulp-sourcemaps');
const merge = require('merge2');
const tslint = require('gulp-tslint');
const tsconfig = require('./tsconfig.json');

const srcFiles = [
    './src/**/*.ts'
];
const outDir = 'out';

gulp.task('clean', function () {
    return del([
        outDir + '/**'
    ]);
});

gulp.task('tslint:src', function () {
    return gulp.src(srcFiles)
        .pipe(tslint({
            formatter: 'verbose'
        }))
        .pipe(tslint.report({
            summarizeFailureOutput: true
        }));
});

gulp.task('compile:src', gulp.series('tslint:src', function () {
    var tsResult = gulp.src(srcFiles, {
        base: './src'
    })
        .pipe(sourcemaps.init())
        .pipe(typescript(tsconfig.compilerOptions))
        .on('error', function (err) {
            throw err;
        });

    return merge([
        tsResult.dts.pipe(gulp.dest(outDir)),
        tsResult.js
            .pipe(sourcemaps.mapSources(function (sourcePath, file) {
                return '../src/' + sourcePath;
            }))
            .pipe(sourcemaps.write('.', {
                includeContent: false,
                sourceRoot: ''
            }))
            .pipe(gulp.dest(outDir)),
        tsResult.js.pipe(gulpDebug({ title: 'copied:' }))
    ]);
}));

gulp.task('copy:war', function () {
    return gulp.src(['./src/war/**'], {
        base: 'src'
    })
        .pipe(gulp.dest(outDir));
});

gulp.task('build', gulp.series('clean', 'compile:src', 'copy:war', function (cb) {
    cb();
}));

gulp.task('default', gulp.series('build'));
