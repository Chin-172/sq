// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@

import { Container } from 'inversify';
import { DIConfig as BaseDIConfig } from 'webapp-endpoint';
import { AppConfigurationValidator } from '../appConfigurationValidator';
import Identifiers from './identifiers';

/**
 * The dependency injection configurator for the app.
 */
export class DIConfig {
    /**
     * Register the classes in the DI Container.
     *
     * @param {string} appAliasKey The alias for the app to read the configurations for.
     * @param {string[]} appConfigNames The names of additional app specific configurations to read.
     * @returns {Container} The DI container.
     */
    public static async register(appAliasKey: string, appConfigNames: string[]): Promise<Container> {
        // initialize the DI container
        let container = await BaseDIConfig.register(appAliasKey, appConfigNames);

        // configuration
        container.bind<AppConfigurationValidator>(Identifiers.AppConfigurationValidator).to(AppConfigurationValidator);

        // services

        return container;
    }
}
