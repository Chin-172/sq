// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@

/**
 * The identifiers for DI used by inversifyjs.
 */
module Identifiers {
    /**
     * The identifier for 'AppConfigurationValidator'.
     */
    export const AppConfigurationValidator = Symbol('AppConfigurationValidator');
}

export default Identifiers;
