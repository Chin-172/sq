// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@
/* eslint-disable */

import * as express from 'express';
import { Container, inject, injectable } from 'inversify';
import { BaseConfigParams, BaseIdentifiers, IStartupConfig, WebappEndpoint } from 'webapp-endpoint';
import { AppConfigurationValidator } from './appConfigurationValidator';
import Identifiers from './DI/identifiers';

import { proxy, config, middleware, log, httpsServer } from '@galaxy-next/web-server';

/* eslint-enable */

/**
 * The server startup class.
 */
@injectable()
export class Server {
    /**
     * An instance of the webapp endpoint class.
     */
    @inject(BaseIdentifiers.WebappEndpoint)
    private webappEndpoint: WebappEndpoint;

    /**
     * The configuration object.
     */
    @inject(BaseIdentifiers.BaseConfigParams)
    private configParams: BaseConfigParams;

    /**
     * An instance of the configuration validator class for the app.
     */
    @inject(Identifiers.AppConfigurationValidator)
    private appConfigurationValidator: AppConfigurationValidator;

    // @inject(LoggerIdentifiers.ServiceLogger)
    // private logger: any;

    // Run configuration methods on the Express instance.
    public async start(port: number, appAliasKey: string, container: Container): Promise<void> {
        // const whitelistedSources = this.configParams.getValue('WHITELISTED_SOURCES').split(',');

        // Get backend config.
        const backendConfig = config.getBackendConfig();

        // initialize the start up configuration
        const startupConfig: IStartupConfig = {
            authenticationPaths: [
                '*'
            ],
            clientRoutes: backendConfig.clientRoutes,
            configValidator: this.appConfigurationValidator,
            container: container,
            controllers: [
            ],
            customHelmetOptions: {
                contentSecurityPolicy: {
                    // disable lint for double quotes
                    // tslint:disable
                    directives: config.getDirectives()
                    // {
                    //     'connect-src': ["'self'", ...whitelistedSources],
                    //     'img-src': ["'self'", 'data:', 'blob:', ...whitelistedSources],
                    //     'script-src': ["'self'", "'unsafe-inline'", "'unsafe-eval'", ...whitelistedSources],
                    //     'style-src': ["'self'", "'unsafe-inline'", ...whitelistedSources],
                    // }
                    // tslint:enable
                }
            },
            loggerConfig: {
                eventSource: 'galaxy-sample-app',
                eventSourceVersion: '1.0.0',
                eventVersion: '1.0.0'
            },
            postLogoutUrl: this.configParams.getValue('POST_LOGOUT_URL'),
            publicRoutes: backendConfig.publicRoutes,
            queryParams: {
                allowSignup: 'false',
                zone: null
            },
            scopes: backendConfig.scopes,
            staticPaths: [
                // './war/'
                ...backendConfig.staticPaths
            ],
            useBodyParser: false
        };

        await this.webappEndpoint.start(port, appAliasKey, startupConfig);
        let app: express.Application = this.webappEndpoint.getExpressApp();

        // Setup galaxy middleware.
        middleware.galaxyMiddleware(this.webappEndpoint);

        // Setup log server for TEA test.
        log.logServer(app);

        // Setup proxy.
        proxy.atlasProxy(app);
        proxy.clearanceProxy(app);
        proxy.galaxyProxy(app);

        // Start HTTPs server.
        await httpsServer.start(app, 4010);
    }

    public stop() {
        this.webappEndpoint.stop();
    }
}
