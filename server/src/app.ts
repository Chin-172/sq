// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@
// <summary>Contains the basic functionality for starting up a Node.js express web server</summary>

import { DIConfig } from './DI/diConfig';
import { Server } from './server';

import { config } from '@galaxy-next/web-server';

(async () => {
    await config.load();
    const port = +process.argv[2] || 4000;
    const appAliasKey = 'galaxy-sample-app';
    const appConfigNames = [
        'POST_LOGOUT_URL',
        'WHITELISTED_SOURCES'
    ];

    // build the dependency injection container
    let container = await DIConfig.register(appAliasKey, appConfigNames);

    // build the instance of the server from the DI container
    let server = container.resolve(Server);

    // start the server
    await server.start(port, appAliasKey, container);
})().catch((err) => {
    console.log('Exiting app with error...', err);
    process.exit(1);
});
