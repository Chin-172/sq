// @<COPYRIGHT>@
// ==================================================
// Copyright 2021.
// Siemens Product Lifecycle Management Software Inc.
// All Rights Reserved.
// ==================================================
// @<COPYRIGHT>@
/* eslint-disable */

import { injectable } from 'inversify';
import { isEmpty, isNil, trim } from 'lodash';
import { BaseConfigParams, IConfigValidator } from 'webapp-endpoint';

/* eslint-enable */

/**
 * The configuration class for the app.
 */
@injectable()
export class AppConfigurationValidator implements IConfigValidator {
    /**
     * Validate configuration parameters.
     *
     * @param {BaseConfigParams} configParams The configuration parameters.
     */
    public validate(configParams: BaseConfigParams) {
        const invalidParams: string[] = [];

        this.validateConfigParam(configParams, 'POST_LOGOUT_URL', invalidParams);

        if (!isEmpty(invalidParams)) {
            throw new Error('The following configuration parameters are missing: ' + invalidParams.join());
        }
    }

    /**
     * Validate specified configuration parameter.
     *
     * @param {BaseConfigParams} configParams The configuration parameters.
     * @param {string} configName The name of the configuration parameter to validate.
     * @param {string[]} invalidParams The list of invalid configuration parameters to update.
     */
    private validateConfigParam(configParams: BaseConfigParams, configName: string, invalidParams: string[]) {
        const configValue: any = configParams.getValue(configName);
        if (isNil(configValue) ||
            (typeof configValue === 'string' && isEmpty(trim(configValue))) ||
            (Array.isArray(configValue) && isEmpty(configValue))) {
            invalidParams.push(configName);
        }
    }
}
