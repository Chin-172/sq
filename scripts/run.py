# Use this to generate regular commands or cascade commands.
# It's messy but it reduces mistakes a lot.

partDesignCommands = ["Extrude", "Revolve","Sweep","Loft","Measure","EdgeBlend","Chamfer","Draft","Hole","Shell","MirrorFeature","PatternFeature"]

pmiCommands = [
    "Feature",
    "Rapid",
    "FCF",
    "DatumPlane",
    "Target",
    "Note",
    "View",
    "Publish"
    ]

commands = pmiCommands

assembliesCommands = ["Undo", "Redo", "Delete", "Hide"]
assembliesGroupData = [None, None, ["Delete", "Cut", "Copy", "Paste"], ["Hide", "Show", "ImmediateHide", "ShowAndHide"]]

sketchOtherCommands = ["Delete", "Hide"]
sketchGroupCommands = [["Delete", "Cut", "Copy", "Paste"], ["Hide", "Show", "ImmediateHide", "ShowAndHide"]]
partDesignOtherCommands = ["Delete", "Hide", "Sketch", "Unite", "DatumPlane"]
partDesignGroupCommands = [["Delete", "Cut", "Copy", "Paste"], ["Hide", "Show", "ImmediateHide", "ShowAndHide"], ["SketchOriginPlane", "SketchFace", "SketchPlane"], ["Unite", "Subtract", "Intersect"], ["DatumPlane", "DatumAxis", "DatumPoint"]]

sketchMultis = ["InferDimension", "Include"]
sketchMultisData = [["InferDimension"], ["Include", "ProjectCurve", "Reattach"]]

def run(prefix, commands):
    for command in commands:
        name = command
        print(f'"{prefix}{name}ButtonCommand": "",')

    print("===")

    for command in commands:
        name = command
        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"view{prefixUpper}{name}Title": "",')
        print(f'"view{prefixUpper}{name}Description": "",')

    print("===")

    for command in commands:
        name = command
        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"{prefix}{name}ButtonCommand":{{')
        print(f'"iconId": "uxCloudCreatePmi",')
        print(f'"title": "{{{{i18n.view{prefixUpper}{name}Title}}}}",')
        print(f'"description": "{{{{i18n.view{prefixUpper}{name}Description}}}}"')
        print(f'}},')

    print("===")

    for command in commands:
        name = command
        print(f'"{prefix}{name}ButtonCommandHandler":{{')
        print(f'"id": "{prefix}{name}ButtonCommand",')
        print(f'"action": "{prefix}{name}ButtonCommandAction",')
        print(f'"activeWhen": true,')
        print(f'"visibleWhen": true')
        print(f'}},')

    print("===")

    for command in commands:
        name = command
        print(f'"{prefix}{name}ButtonCommandPlacement":{{')
        print(f'"id": "{prefix}{name}ButtonCommand",')
        print(f'"uiAnchor": "{prefix}Ribbon",')
        print(f'"priority": 1')
        print(f'}},')

    print("===")

    for command in commands:
        name = command
        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"{prefix}{name}ButtonCommandAction":{{')
        print(f'"actionType": "JSFunction",')
        print(f'"method": "{nameLower}Action",')
        print(f'"deps": "js/part{prefixUpper}Toolbar"')
        print(f'}},')

    for command in commands:
        name = command
        nameLower = name[0].lower() + name[1:]
        print(f'export const {nameLower}Action = function() {{')
        print(f'console.log(\'This is the {name} action\');')
        print(f'}};')


    for command in commands:
        name = command
        nameLower = name[0].lower() + name[1:]
        print(f'{nameLower}Action,')

    for command in commands:
        name = command
        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"view{prefixUpper}{name}Title": [')
        print(f'"galaxyAppfactoryMessages"],')
        print(f'"view{prefixUpper}{name}Description": [')
        print(f'"galaxyAppfactoryMessages"],')

def runGroupCommands(prefix, commands, groupData):
    for command in commands: #DONE
        name = command
        print(f'"{prefix}{name}ButtonCommand": "",')

    print("===")

    for i in range(0, len(commands)): #Done
        name = commands[i]
        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"view{prefixUpper}{name}Title": "",')
        print(f'"view{prefixUpper}{name}Description": "",')

        if groupData[i]:
            for name in groupData[i]:
                nameLower = name[0].lower() + name[1:]
                prefixUpper = prefix[0].upper() + prefix[1:]
                print(f'"view{prefixUpper}{name}OptionTitle": "",')
                print(f'"view{prefixUpper}{name}OptionDescription": "",')

    print("===")

    for i in range(0, len(commands)): # DONE
        command = commands[i]
        name = command
        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"{prefix}{name}ButtonCommand":{{')
        print(f'"iconId": "uxCloudCreatePmi",')
        print(f'"title": "{{{{i18n.view{prefixUpper}{name}Title}}}}",')
        if groupData[i]:
            print(f'"description": "{{{{i18n.view{prefixUpper}{name}Description}}}}",')
            print(f'"isGroup": true')
        else:
            print(f'"description": "{{{{i18n.view{prefixUpper}{name}Description}}}}"')
        print(f'}},')

        if groupData[i]:
            for name in groupData[i]:
                nameLower = name[0].lower() + name[1:]
                prefixUpper = prefix[0].upper() + prefix[1:]
                print(f'"{prefix}{name}OptionButtonCommand":{{')
                print(f'"iconId": "uxCloudCreatePmi",')
                print(f'"title": "{{{{i18n.view{prefixUpper}{name}OptionTitle}}}}",')
                print(f'"description": "{{{{i18n.view{prefixUpper}{name}OptionDescription}}}}"')
                print(f'}},')

    print("===")

    for i in range(0, len(commands)): #DONE
        command = commands[i]
        name = command
        print(f'"{prefix}{name}ButtonCommandHandler":{{')
        print(f'"id": "{prefix}{name}ButtonCommand",')
        print(f'"activeWhen": true,')
        print(f'"visibleWhen": true')
        print(f'}},')

        if groupData[i]:
            for name in groupData[i]:
                print(f'"{prefix}{name}OptionButtonCommandHandler":{{')
                print(f'"id": "{prefix}{name}OptionButtonCommand",')
                print(f'"action": "{prefix}{name}OptionButtonCommandAction",')
                print(f'"activeWhen": true,')
                print(f'"visibleWhen": true')
                print(f'}},')


    print("===")

    for i in range(0, len(commands)): #DONE
        command = commands[i]
        name = command
        print(f'"{prefix}{name}ButtonCommandPlacement":{{')
        print(f'"id": "{prefix}{name}ButtonCommand",')
        print(f'"uiAnchor": "{prefix}Ribbon",')
        print(f'"priority": 1')
        print(f'}},')

        if groupData[i]:
            for name in groupData[i]:
                print(f'"{prefix}{name}OptionButtonCommandPlacement":{{')
                print(f'"id": "{prefix}{name}OptionButtonCommand",')
                print(f'"parentGroupId": "{prefix}{command}ButtonCommand",')
                print(f'"priority": 1')
                print(f'}},')

    print("===")

    for i in range(0, len(commands)): # DONE
        command = commands[i]

        if not groupData[i]:
            name = command
            nameLower = name[0].lower() + name[1:]
            prefixUpper = prefix[0].upper() + prefix[1:]
            print(f'"{prefix}{name}ButtonCommandAction":{{')
            print(f'"actionType": "JSFunction",')
            print(f'"method": "{nameLower}Action",')
            print(f'"deps": "js/part{prefixUpper}Toolbar"')
            print(f'}},')
        else:
            for name in groupData[i]:
                nameLower = name[0].lower() + name[1:]
                prefixUpper = prefix[0].upper() + prefix[1:]
                print(f'"{prefix}{name}OptionButtonCommandAction":{{')
                print(f'"actionType": "JSFunction",')
                print(f'"method": "{nameLower}OptionAction",')
                print(f'"deps": "js/part{prefixUpper}Toolbar"')
                print(f'}},')


    for i in range(0, len(commands)): # DOne
        command = commands[i]

        if groupData[i]:
            for name in groupData[i]:
                nameLower = name[0].lower() + name[1:]
                print(f'export const {nameLower}OptionAction = function() {{')
                print(f'console.log(\'This is the {name} action\');')
                print(f'}};')
        else:
            name = command
            nameLower = name[0].lower() + name[1:]
            print(f'export const {nameLower}Action = function() {{')
            print(f'console.log(\'This is the {name} action\');')
            print(f'}};')



    for i in range(0, len(commands)): # DOne

        name = commands[i]

        if groupData[i]:
            for name in groupData[i]:
                nameLower = name[0].lower() + name[1:]
                print(f'{nameLower}OptionAction,')
        else:
            nameLower = name[0].lower() + name[1:]
            print(f'{nameLower}Action,')

    for i in range(0, len(commands)): # DOne
        name = commands[i]

        nameLower = name[0].lower() + name[1:]
        prefixUpper = prefix[0].upper() + prefix[1:]
        print(f'"view{prefixUpper}{name}Title": [')
        print(f'"galaxyAppfactoryMessages"],')
        print(f'"view{prefixUpper}{name}Description": [')
        print(f'"galaxyAppfactoryMessages"],')

        if groupData[i]:
            for name in groupData[i]:
                nameLower = name[0].lower() + name[1:]
                prefixUpper = prefix[0].upper() + prefix[1:]
                print(f'"view{prefixUpper}{name}OptionTitle": [')
                print(f'"galaxyAppfactoryMessages"],')
                print(f'"view{prefixUpper}{name}OptionDescription": [')
                print(f'"galaxyAppfactoryMessages"],')



#run("createPMI")
runGroupCommands("sketch", sketchMultis, sketchMultisData)
