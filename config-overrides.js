/* eslint-env node */

const afx = require( '@swf/core/config-overrides' );
const path = require( 'path' );
const fs = require( 'fs' );

const { addExcludeFileTypeToFileLoader } = require( '@swf/tooling/rewired/utils' );

module.exports = {
    webpack: ( config ) => {
        let resultantConfig = afx.webpack( config );

        const glslRegex = /\.glsl$/i;

        resultantConfig = addExcludeFileTypeToFileLoader(
            resultantConfig,
            glslRegex
        );

        resultantConfig.module.rules.unshift( {
            test: glslRegex,
            use: [ { loader: 'raw-loader' } ]
        } );

        if( !resultantConfig.resolve ) {
            resultantConfig.resolve = {};
        }
        if( !resultantConfig.resolve.alias ) {
            resultantConfig.resolve.alias = {};
        }
        // aliasing galaxy/swf/atlas packages to the version installed
        // by starlight
        [ '@swf3d', '@swf', '@atlas', '@com.siemens.plm.web' ].forEach(
            ( scope ) =>
            fs.readdirSync( path.join( __dirname, 'node_modules', scope ) )
            .forEach( ( pkg ) =>
                resultantConfig.resolve.alias[ `${scope}/${pkg}` ] = path.join( __dirname, 'node_modules', scope, pkg )
            )
        );

        if( fs.existsSync( path.join( __dirname, 'node_modules', '@com.siemens.plm.web/plmvisweb-private' ) ) ) {
            resultantConfig.resolve.alias[ '@com.siemens.plm.web/PLMVisWeb' ] =
                path.join( __dirname, 'node_modules', '@com.siemens.plm.web/plmvisweb-private' );
        }

        return resultantConfig;
    },
    devServer: ( configFunction ) => {
        return ( proxy, allowedHost ) => {
            let afxConfigFunction = afx.devServer( configFunction );
            let config = afxConfigFunction( proxy, allowedHost );
            return config;
        };
    },
    jest: ( config ) => {
        let resultantConfig = afx.jest( config );
        return resultantConfig;
    },
    paths: ( paths ) => {
        return afx.paths( paths );
    }
};
